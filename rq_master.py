from rq import Queue
from redis import Redis
from app import ClearGeodeticScans
from time import sleep
from database import session, Images
import log
import glob
import os.path


logger = log.set_logger("main", "DEBUG")
redis_conn = Redis(host="10.0.0.16", port=6379)
q = Queue(connection=redis_conn)


cleaner = ClearGeodeticScans(source_path="/srv/docker/cgs/p",
                             destination_path="/srv/docker/cgs/done",
                             debug=True,
                             debug_path="/srv/docker/cgs/debug"
                             )

all_jobs = []

# add files to DB
# count = 0
# for filename in glob.iglob('files/p/**', recursive=True):
#     if count % 50 == 0:
#         session.commit()
#         count = 0
#     if os.path.isfile(filename):
#         filename = filename.replace("files/p\\", "")
#         session.add(Images(
#             name=filename
#         ))
#         logger.debug("Add to session:\t{}".format(filename))
#
#     count += 1
# session.commit()



# run OCR
# for image in session.query(Images):
#     print(image.id)
#     all_jobs.append(q.enqueue(cleaner.ocr, image.id))



# run compare
all_images = session.query(Images)
for image in all_images:
    logger.info("Image id:\t{}".format(image.id))
    all_jobs.append(q.enqueue(cleaner.text_compare, image))


# run draw
# all_images = session.query(Images)
# for image in all_images:
#     logger.info("Image id:\t{}".format(image.id))
#     all_jobs.append(q.enqueue(cleaner.draw, image.id, color=(214, 134, 85)))


# for job_id in q.job_ids:
#     print(job_id)


# TODO: add rq_master docker container
# TODO: add sqlite for files list and mark jobs as done
# TODO: first add all files to database and later update each instead of inserting
