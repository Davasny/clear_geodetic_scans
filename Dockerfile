FROM ubuntu:18.04

RUN apt-get update && apt-get install -y tesseract-ocr libtesseract-dev
RUN apt-get install -y python3-pip
RUN apt-get install -y tesseract-ocr-pol
RUN apt-get install -y libmysqlclient-dev

WORKDIR /usr/src/clear_geodetic_scans

ADD ./requirements-docker.txt /usr/src/clear_geodetic_scans/requirements-docker.txt
RUN pip3 install -r requirements-docker.txt

ADD . /usr/src/clear_geodetic_scans

ARG CGS_DB_USER
ARG CGS_DB_PASS
ARG CGS_DB_HOST
ARG CGS_DB_PORT
ARG CGS_DB_DB

ENV CGS_DB_USER=$CGS_DB_USER
ENV CGS_DB_PASS=$CGS_DB_PASS
ENV CGS_DB_HOST=$CGS_DB_HOST
ENV CGS_DB_PORT=$CGS_DB_PORT
ENV CGS_DB_DB=$CGS_DB_DB

# TODO: add redis docker container
CMD export LC_ALL=C.UTF-8 && export LANG=C.UTF-8 && rq worker --url redis://:@10.0.0.16:6379/
