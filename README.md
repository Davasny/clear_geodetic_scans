# clear_geodetic_scans

```bash
export CGS_DB_USER=
export CGS_DB_PASS=
export CGS_DB_HOST=
export CGS_DB_PORT=
export CGS_DB_DB=

docker-compose -p cgs build --build-arg CGS_DB_USER=$CGS_DB_USER --build-arg CGS_DB_PASS=$CGS_DB_PASS --build-arg CGS_DB_HOST=$CGS_DB_HOST --build-arg CGS_DB_PORT=$CGS_DB_PORT --build-arg CGS_DB_DB=$CGS_DB_DB

docker-compose -p cgs up -d --scale worker=2
```