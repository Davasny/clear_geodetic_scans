from sqlalchemy import Column, Integer, text, ForeignKey
from sqlalchemy import create_engine
from sqlalchemy.engine import url
from sqlalchemy.dialects.mysql import BOOLEAN, TIME, TEXT, VARCHAR
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import config

Base = declarative_base()


class Images(Base):
    __tablename__ = 'images'
    id = Column(Integer, primary_key=True, index=True)
    name = Column(VARCHAR(255), nullable=False)
    size = Column(Integer)
    width = Column(Integer)
    height = Column(Integer)
    elapsed_time = Column(TIME)


class Texts(Base):
    __tablename__ = 'texts'
    id = Column(Integer, primary_key=True, index=True)
    image_id = Column(Integer, ForeignKey("images.id"), nullable=False, index=True)
    x = Column(Integer)
    y = Column(Integer)
    width = Column(Integer)
    height = Column(Integer)
    content = Column(TEXT)
    overwrite = Column(BOOLEAN, default=False)


engine_url = url.URL(
    drivername='mysql',
    host=config.MYSQL['host'],
    port=config.MYSQL['port'],
    username=config.MYSQL['user'],
    password=config.MYSQL['password'],
    query={'charset': 'utf8'}
)

engine = create_engine(engine_url, encoding='utf-8')
engine.execute("CREATE DATABASE IF NOT EXISTS {} character set UTF8 collate utf8_bin;".format(config.MYSQL['db']))
engine.execute("USE {};".format(config.MYSQL['db']))

Base.metadata.create_all(engine)
DBSession = sessionmaker(bind=engine)
session = DBSession()
