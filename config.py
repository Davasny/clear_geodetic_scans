import os


MYSQL = {
    "user": os.environ['CGS_DB_USER'],
    "password": os.environ['CGS_DB_PASS'],
    "host": os.environ['CGS_DB_HOST'],
    "port": os.environ['CGS_DB_PORT'],
    "db": os.environ['CGS_DB_DB']
}
