import numpy as np
import cv2
import os.path
import pytesseract
import csv
import log
import re
import glob
from database import session, Images, Texts

logger = log.set_logger("main", "DEBUG")


class ClearGeodeticScans:
    def __init__(self,
                 source_path=None,
                 destination_path=None,
                 debug_path=None,
                 debug=True):
        self._source_path = source_path
        self._destination_path = destination_path
        self._debug = debug
        self._debug_path = debug_path
        
        logger.info("Initialized CGS with settings:\n"
                    "\tsource_path:\t{}\n"
                    "\tdestination_path:\t{}\n"
                    "\tdebug:\t{}\n"
                    "\tdebug_path:\t{}".format(source_path, destination_path, debug, debug_path)
                    )

    def _make_dir_path(self, filename, dirname):
        """
        Function creates full directory tree based on filename with directories

        :param filename: eg dir1/dir2/1.jpg
        :param dirname: eg debug
        :return:
        """
        edges_files_path = "{}/{}/{}".format(self._debug_path, dirname, "/".join(filename.split("/")[:-1]))
        if not os.path.exists(edges_files_path):
            os.makedirs(edges_files_path, exist_ok=True)  # mkdir -p
        return True

    def ocr(self, image_id):
        """
        Function detects lines and texts in the image. If debug
        enabled - draws scrapped informations into debug file

        Required:
        self._source_path - /srv/cgs/files
        self._debug_path - /srv/cgs/debug
        self._debug - True/False - draw debug lines

        :param filename: {self.source_path}/{filename} (eg. dir1/dir3/1.jpg)
        :return:
        """
        image = session.query(Images).filter(Images.id == image_id).first()
        filename = image.name

        filename = filename.replace("\\", "/")

        full_file_path = "{}/{}".format(self._source_path, filename)
        logger.info("Loading:\t{}".format(full_file_path))

        # read the image and get the dimensions
        img = cv2.imread(full_file_path)

        orig = img.copy()  # create copy of original image
        img_height, img_width, channels = img.shape  # assumes color image

        # run tesseract, returning all texts with positions
        logger.info("Running tesseract")
        # TODO: make two OCR iterations for PSM 1 and 12
        # merge texts from both OCRs

        start_x = int(img_width / 2)

        image_data = pytesseract.image_to_data(img[
                                               0:img_height,
                                               start_x:img_width,
                                               ], lang="pol", config='--psm 12')

        csv_dict = csv.DictReader(image_data.splitlines(), delimiter="\t")
        image_surface = img_height * (img_width - start_x)

        # create new Image object for DB purposes
        image.width = img_width
        image.height=img_height

        session.add(image)
        session.flush()

        if self._debug:
            # create new blank file with white background for debug
            blank_image = np.zeros((img_height, img_width, 3), np.uint8)
            blank_image[::] = (255, 255, 255)

        for line in csv_dict:
            x = start_x + int(line["left"])
            y = int(line["top"])
            w = int(line["width"])
            h = int(line["height"])
            text = line["text"]

            rectangle_surface = w * h

            #  check if rectangle is not full screen
            if rectangle_surface / image_surface < 0.5 and text is not None:
                if text.strip() != "":
                    #  create new text object and add to DB session
                    session.add(
                        Texts(
                            image_id=image.id,
                            x=x,
                            y=y,
                            width=w,
                            height=h,
                            content=text
                        )
                    )

                if self._debug:
                    # draw detected rectangle in blank image
                    blank_image = cv2.rectangle(blank_image, (x, y), (x + w, y + h), (0, 0, 255))

                    # draw text in blank image
                    blank_image = cv2.putText(
                        blank_image,
                        text,
                        (x, int(y + h)),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        0.5,
                        (0, 0, 0)
                    )

        if self._debug:
            # detect edges and save
            logger.debug("Looking for lines")
            edges = cv2.Canny(orig, 60, 120, apertureSize=3)

            logger.debug("Writing to edges file")

            self._make_dir_path(filename, "edges")
            cv2.imwrite("{}/edges/{}".format(self._debug_path, filename), edges)

            lines = cv2.HoughLinesP(
                image=edges,
                rho=1,
                theta=np.pi / 2,
                lines=np.array([]),
                threshold=550,
                minLineLength=80,
                maxLineGap=50
            )
            if lines is not None:
                a, b, c = lines.shape
                logger.debug("Found lines:\t{}".format(a))
                for i in range(a):
                    cv2.line(
                        blank_image,
                        (lines[i][0][0], lines[i][0][1]),
                        (lines[i][0][2], lines[i][0][3]),
                        (0, 0, 0),
                        1,
                        cv2.LINE_AA
                    )

            self._make_dir_path(filename, "lines")
            cv2.imwrite("{}/lines/{}".format(self._debug_path, filename), blank_image)

        session.commit()
        return True

    def text_compare(self, image):
        """
        Function takes all texts from DB for specified image_id and makes comparasion on them

        :param image: image object from DB
        :return:
        """
        texts = session.query(Texts).filter(Texts.image_id == image.id)

        texts_by_x = {}
        for text in texts:
            if text.x not in texts_by_x:
                texts_by_x[text.x] = {}
            texts_by_x[text.x][text.y] = text

        ids_to_update = []

        for text in texts:
            text.overwrite = False
            if text.x > 0.5 * image.width and text.y > 0.25 * image.height and text.y < 0.5 * image.height:
                text_content = text.content.lower()
                if re.match(r"(amster|.*rdam|poziom|odniesienia|wysokości)", text_content):
                    logger.debug("Word matched in 1:\t{}".format(text.content))
                    ids_to_update.append(text.id)
                elif re.match(r"(kronszt)", text_content):
                    if re.match(r"[86][86]", text_content):
                        logger.debug("Word matched in 2:\t{}".format(text.content))
                        ids_to_update.append(text.id)
                    else:
                        for check_x in range(text.x + 1, text.x + 300):
                            if check_x in texts_by_x:
                                for check_y in range(text.y - text.height, int(text.y + text.height)):
                                    if check_y in texts_by_x[check_x]:
                                        if re.match(r"[86][86]", texts_by_x[check_x][check_y].content):
                                            logger.debug(
                                                "Word matched in 3:\t{} -> {}".format(
                                                    text.content, texts_by_x[check_x][check_y].content))
                                            ids_to_update.append(text.id)
                                            ids_to_update.append(texts_by_x[check_x][check_y].id)

                elif re.match(r"(h\s*[=\-:;7])", text_content):
                    if re.match(r"(.*\d{3}\.\d{2})", text_content):
                        # full correct number
                        logger.debug("Word matched in 4:\t{}".format(text.content))
                        ids_to_update.append(text.id)
                    else:
                        s1 = False
                        if re.match(r".*\d{3}", text_content):
                            # h=123
                            s1 = True
                        s2 = False
                        s3 = False
                        for check_x in range(text.x + 1, text.x + 300):
                            if check_x in texts_by_x:
                                for check_y in range(text.y - text.height, int(text.y + text.height)):
                                    if check_y in texts_by_x[check_x]:

                                        if re.match(r"\d{3}", texts_by_x[check_x][check_y].content):
                                            s2 = True
                                        elif re.match(r"\d{2}", texts_by_x[check_x][check_y].content):
                                            s3 = True

                                        if (s2 and s3) or (s1 and s3):
                                            logger.debug(
                                                "Word matched in 5 "
                                                "(can skip some important string in display):\t{} -> {}".format(
                                                    text.content, texts_by_x[check_x][check_y].content))
                                            ids_to_update.append(text.id)
                                            ids_to_update.append(texts_by_x[check_x][check_y].id)

                elif re.match(r"[h\|n:;+\*]|(<=)", text_content):
                    s2 = False
                    s3 = False
                    s4 = False
                    for check_x in range(text.x + 1, text.x + 300):
                        if check_x in texts_by_x:
                            for check_y in range(text.y - text.height, int(text.y + text.height)):
                                if check_y in texts_by_x[check_x]:
                                    if (re.match(r"[=e7,\-+\*]", texts_by_x[check_x][check_y].content) and
                                            len(texts_by_x[check_x][check_y].content) < 5):
                                        # found "="
                                        s2 = True
                                        ids_to_update.append(texts_by_x[check_x][check_y].id)
                                        logger.debug("Found \"=\" in:\t{}".format(texts_by_x[check_x][check_y].content))
                                    elif re.match(r"\d{3}[\.,]\d{2}", texts_by_x[check_x][check_y].content) and s2:
                                        # found 123.45
                                        s3 = True
                                        s4 = True
                                        ids_to_update.append(texts_by_x[check_x][check_y].id)
                                    elif re.match(r"(\d{3}|\d{4})\s?\.?", texts_by_x[check_x][check_y].content) and \
                                            s2 and not s3 and not s4:
                                        # found 123. or 123
                                        s3 = True
                                        ids_to_update.append(texts_by_x[check_x][check_y].id)
                                    elif re.match(r"\d{2}", texts_by_x[check_x][check_y].content) and s3 and not s4:
                                        # 12
                                        s4 = True
                                        ids_to_update.append(texts_by_x[check_x][check_y].id)

                                    if s2 and (s3 or s4):
                                        logger.debug(
                                            "[done] Word matched in 6 "
                                            "(can skip some important string in display):\t{} -> {}".format(
                                                text.content, texts_by_x[check_x][check_y].content))
                                        ids_to_update.append(text.id)

        for id in ids_to_update:
            session.query(Texts).filter(Texts.id == id).first().overwrite = True
        session.commit()

        logger.info("Matched:\t{}".format(len(ids_to_update)))

        return True

    def draw(self, image_id, color=(255, 255, 255)):
        """
        Function draws pixels in place of texts that are marked as overwrite=True

        Required:
        self.source_path - /srv/cgs/files
        self.destination_path - /srv/cgs/cleared
        self.debug - True/False

        :param image_id: ID from database
        :param color: color in (B,G,R)
        :return:
        """
        image = session.query(Images).filter(Images.id == image_id).first()
        filename = image.name

        filename = filename.replace("\\", "/")

        full_file_path = "{}/{}".format(self._source_path, filename)
        logger.info("Drawing on:\t{}".format(full_file_path))
        img = cv2.imread(full_file_path)

        texts = session.query(Texts).filter(Texts.image_id == image_id, Texts.overwrite == True)

        for text in texts:
            img = cv2.rectangle(
                img,
                (text.x, text.y),
                (text.x + text.width, text.y + text.height),
                color,
                -1
            )

        # self._make_dir_path(filename, "")
        self._make_dir_path(filename, "done")
        cv2.imwrite("{}/done/{}".format(self._debug_path, filename), img)

        return True

    def merge_blue_ang_white(self):
        dirs = ["I", "O"]
        white = "whites/"
        for dir in dirs:
            for filename in glob.iglob('debug/done/**/{}/**'.format(dir), recursive=True):
                if os.path.isfile(filename):
                    white_file = white + filename.replace(dir + "\\", "").replace("debug/done\\", "")
                    if os.path.isfile(white_file):
                        print("{}\t->\t{}".format(white_file, filename))
                        os.remove(filename)
                        os.rename(white_file, filename)


    def replace_destroyed(self):
        source_path = 'files/'
        dirs = ["P", "R"]
        for dir in dirs:
            for filename in glob.iglob('debug/done/**/{}/**'.format(dir), recursive=True):
                if os.path.isfile(filename):
                    white_file = source_path + filename.replace(dir + "\\", "").replace("debug/done\\", "")
                    if os.path.isfile(white_file):
                        print("{}\t->\t{}".format(white_file, filename))
                        os.remove(filename)
                        os.rename(white_file, filename)


if __name__ == '__main__':
    import glob

    cleaner = ClearGeodeticScans(source_path="files", destination_path="done", debug=True, debug_path="debug")



    # cleaner.ocr(804)
    # cleaner.ocr(850)
    # cleaner.ocr(865)

    # id = 1060
    # cleaner.ocr(id)

    all_images = session.query(Images)
    for image in all_images:
        logger.info("Image id:\t{}".format(image.id))
        # cleaner.text_compare(image)
        cleaner.draw(image.id, color=(214, 134, 85))

    # cleaner.draw(id, color=(214, 134, 85))
